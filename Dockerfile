FROM python:3.10.5-slim-buster AS build
WORKDIR /app
COPY requirements_app.txt ./requirements.txt
RUN pip install -r requirements.txt
EXPOSE 8501
COPY project model.pkl ./
HEALTHCHECK CMD curl --fail http://localhost:8501/_stcore/health

ENTRYPOINT ["streamlit", "run", "app.py", "--server.port=8501", "--server.address=0.0.0.0"]
