# Mini MLOps : Simple CI/CD for ML Model

_Afif A. Iskandar_

- email: <afif_a_iskandar@telkomsel.co.id>
- youtube: [NgodingPython](https://youtube.com/NgodingPython)

Repositori ini merupakan project sederhana implementasi CI/CD untuk project _machine learning_


### Variabel yang perlu diset dalam project ini
- `RAILWAY_SERVICE_NAME` : Nama service pada project railways
- `RAILWAY_TOKEN`: Token project yang digunakan di railways
- `REGISTRY_USER`: username akun docker
- `REGISTRY_PASS`: password akun docker
- `REPO_TOKEN`: token akses user gitlab
