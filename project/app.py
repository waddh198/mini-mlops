import pickle

import numpy as np
import pandas as pd
import streamlit as st

cols = ["Pclass", "Age", "SibSp", "Parch", "Fare", "Sex"]

model = pickle.load(open("model.pkl", "rb"))
st.title("Titanic Survival Model :ship:")
pclass = st.selectbox("Pilih Kelas Penumpang", [1, 2, 3])
sex = st.select_slider("Pilih Gender", ["male", "female"])
age = st.number_input("Usia", 0, 70)
sibsp = st.number_input("Pasangan / Saudara ?", 0, 15)
parch = st.number_input("OrangTua / Anak ?", 0, 15)
fare = st.number_input("Harga Tiket", 0, 1000)


def predict():
    row = np.array([pclass, age, sibsp, parch, fare, sex])
    X = pd.DataFrame([row], columns=cols)
    prediction = model.predict(X)
    if prediction[0] == 1:
        st.success("Penumpang Selamat :thumbsup:")
    else:
        st.error("Penumpang Tidak Selamat :thumbsdown:")


trigger = st.button("Predict", on_click=predict)
